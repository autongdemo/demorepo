!contents -R2 -g -p -f -h

!define defaultBrowser {firefox}
!path FitNesseRoot/lib/*.jar
!define TEST_SYSTEM {slim}
!define SLIM_PORT {0}