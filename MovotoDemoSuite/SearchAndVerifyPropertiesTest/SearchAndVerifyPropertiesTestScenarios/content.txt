<<<<<<< HEAD
!|Scenario            |apply advanced search settings and verify|
|add filter conditions                                          |
|pause  execution  for|6                |seconds                |
|verify search results exist                                    |

!|Scenario          |save search and verify                      |
|save search                                                     |
|login with username|testemail744@gmail.com|and password|test@123|
|handle modal                                                    |
|verify login action                                             |
|verify saved search                                             |


!|Scenario         |search for properties in location|location                                 |
|wait  for  element|.//*[@id='homeSearchBox']                                                  |
|clear  text       |.//*[@id='homeSearchBox']                                                  |
|type              |@location                        |into|.//*[@id='homeSearchBox']|text field|
|click             |.//*[@id='homeSearchForm']/a                                               |
|verify search results exist                                                                   |


!|Scenario            |verify search results exist                                                                      |
|variable             |searchResultsExist   |equals  to                                                                 |
|wait  for  element   |(.//div[@class='cardbox'])[1]                                                                    |
|get list source                                                                                                        |
|assert and log result|{$searchResultsExist}|for assert type|POS|with comment|Assert results exist|in page|Verify Result|


!|Scenario         |save search               |
|click             |.//*[@id='linkSaveSearch']|
|wait  for  element|{$LOGIN.usernamefield}    |

!|Scenario            |verify saved search                                                                                                                    |
|click                |.//i[@class='icon-user']/..                                                                                                            |
|pause  execution  for|1               |seconds                                                                                                               |
|click                |.//*[@id='menus']//a[text()='Saved searches']                                                                                          |
|variable             |isSearchSaved   |equals  to                                                                                                            |
|wait  for  element   |(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'title')]                                                                    |
|assert and log result|{$isSearchSaved}|for assert type|POS|with comment|Assert saved search exists                               |in page|Verify Saved Search|
|If                   |{$isSearchSaved}                                                                                                                       |
|variable             |title           |equals  to                                                                                                            |
|get  text  from      |(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'title')]                                                                    |
|variable             |summary         |equals  to                                                                                                            |
|get  text  from      |(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'summary')]                                                                  |
|assert and log result|COMMENT         |for assert type|NA |with comment|Saved search details: Title: {$title} Summary: {$summary}|in page|Saved Search       |
|End If                                                                                                                                                       |



!|Scenario            |login on save                                                                                                                   |
|wait  for  element   |.//iframe[contains(@id,'popupIframe')]                                                                                          |
|select               |.//iframe[contains(@id,'popupIframe')]|frame                                                                                    |
|wait  for  element   |.//*[@id='tbxPsd']                                                                                                              |
|type                 |test@123                              |into           |.//*[@id='tbxPsd']|text field                                            |
|click                |.//*[@id='LoginForm']/div[2]/div[2]/div[4]/span[text()='Login']                                                                 |
|pause  execution  for|3                                     |seconds                                                                                  |
|select               |Default                               |frame                                                                                    |
|variable             |loginSuccess                          |equals  to                                                                               |
|wait  for  element   |.//div[text()='Manage your search']                                                                                             |
|assert and log result|{$loginSuccess}                       |for assert type|POS               |with comment|Assert login success|in page|Verify Login|


!|Scenario            |close save popup modal             |
|wait  for  element   |.//span[text()='Save this search:']|
|pause  execution  for|4             |seconds             |
|click                |.//span[@title='Close']            |
|pause  execution  for|4             |seconds             |

!|Scenario            |get list source                                                                                                     |
|variable             |isMls                                    |equals  to                                                                |
|verify page contains |.//*[@id='disclaimerContainer']/div/ul/li|element                                                                   |
|If                   |{$isMls}                                                                                                            |
|variable             |source                                   |equals  to                                                                |
|get  text  from      |.//*[@id='disclaimerContainer']/div/ul/li                                                                           |
|scroll to            |.//*[@id='disclaimerContainer']/div/ul/li                                                                           |
|assert and log result|COMMENT                                  |for assert type|NA|with comment|List source: {$source}|in page|List Source|
|End If                                                                                                                                    |

!|Scenario                   |remove saved search                                                                                                                 |
|click                       |(.//div[@class='saved ng-scope'])[1]//i[@class='icon-trash-o']                                                                      |
|pause  execution  for       |2                   |seconds                                                                                                        |
|assert and log result       |COMMENT             |for assert type|POS|with comment|Saved search removed from the list                |in page|Remove Saved Search|
|click                       |.//*[@id='menus']//i[contains(@class,'icon-user')]                                                                                  |
|pause  execution  for       |1                   |seconds                                                                                                        |
|click                       |.//*[@id='menus']//a[text()='Saved searches']                                                                                       |
|set page time out           |1                                                                                                                                   |
|variable                    |savedSearchExists   |equals  to                                                                                                     |
|verify page contains element|(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'title')]                                                                 |
|set page time out           |30                                                                                                                                  |
|assert and log result       |{$savedSearchExists}|for assert type|NEG|with comment|Assert saved item is removed and page is refreshed|in page|Verify Saved Search|



!|Scenario            |add filter conditions                                                                                         |
|set price range                                                                                                                     |
|click                |.//label[@for='bedrooms2']                                                                                    |
|click                |.//*[@id='moreFilterBtn']                                                                                     |
|pause  execution  for|3       |seconds                                                                                              |
|click                |.//*[@id='condos']/../i                                                                                       |
|click                |.//label[@for='bath2']                                                                                        |
|select               |1,000   |from           |.//select[@name='minHouseSize']                                                      |
|select               |2,000   |from           |.//select[@name='maxHouseSize']                                                      |
|select               |1+garage|from           |.//select[@name='garage']                                                            |
|select               |1980    |from           |.//select[@name='minYearBuild']                                                      |
|select               |2010    |from           |.//select[@name='maxYearBuild']                                                      |
|assert and log result|COMMENT |for assert type|NA|with comment|After entering advanced filter conditions|in page|Add advanced filter|
|click                |.//*[@id='applyFilters']                                                                                      |

!|Scenario           |handle modal                      |
|variable            |modalExists            |equals  to|
|verify page contains|.//*[@id='returnToMap']|element   |
|If                  |{$modalExists}                    |
|click               |.//*[@id='returnToMap']           |
|End If                                                 |

!|Scenario            |set price range                                                                                                                          |
|scroll to            |.//*[@id='slider-range']/span[2]                                                                                                         |
|click                |.//*[@id='slider-range']/span[2]                                                                                                         |
|loop                 |5       |times                                                                                                                           |
|send robot           |LEFT    |key                                                                                                                             |
|focus and click      |.//*[@id='slider-range']/span[2]                                                                                                         |
|pause  execution  for|3       |seconds                                                                                                                         |
|end  loop                                                                                                                                                      |
|click                |.//*[@id='slider-range']/span[1]                                                                                                         |
|loop                 |5       |times                                                                                                                           |
|send robot           |RIGHT   |key                                                                                                                             |
|focus and click      |.//*[@id='slider-range']/span[1]                                                                                                         |
|pause  execution  for|3       |seconds                                                                                                                         |
|end  loop                                                                                                                                                      |
|variable             |maxPrice|equals  to                                                                                                                      |
|get  text  from      |.//*[@id='priceSlider']/span[2]                                                                                                          |
|variable             |minPrice|equals  to                                                                                                                      |
|get  text  from      |.//*[@id='priceSlider']/span[1]                                                                                                          |
|assert and log result|COMMENT |for assert type|NA|with comment|After setting price range: Max Price: {$maxPrice} Min Price: {$minPrice}|in page|Set Price Range|
=======
!|Scenario            |apply advanced search settings and verify|
|add filter conditions                                          |
|pause  execution  for|6                |seconds                |
|verify search results exist                                    |

!|Scenario          |save search and verify                      |
|save search                                                     |
|login with username|testemail744@gmail.com|and password|test@123|
|handle modal                                                    |
|verify login action                                             |
|verify saved search                                             |


!|Scenario         |search for properties in location|location                                 |
|wait  for  element|.//*[@id='homeSearchBox']                                                  |
|clear  text       |.//*[@id='homeSearchBox']                                                  |
|type              |@location                        |into|.//*[@id='homeSearchBox']|text field|
|click             |.//*[@id='homeSearchForm']/a                                               |
|verify search results exist                                                                   |


!|Scenario            |verify search results exist                                                                      |
|variable             |searchResultsExist   |equals  to                                                                 |
|wait  for  element   |(.//div[@class='cardbox'])[1]                                                                    |
|get list source                                                                                                        |
|assert and log result|{$searchResultsExist}|for assert type|POS|with comment|Assert results exist|in page|Verify Result|


!|Scenario         |save search               |
|click             |.//*[@id='linkSaveSearch']|
|wait  for  element|{$LOGIN.usernamefield}    |

!|Scenario            |verify saved search                                                                                                                    |
|click                |.//i[@class='icon-user']/..                                                                                                            |
|pause  execution  for|1               |seconds                                                                                                               |
|click                |.//*[@id='menus']//a[text()='Saved searches']                                                                                          |
|variable             |isSearchSaved   |equals  to                                                                                                            |
|wait  for  element   |(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'title')]                                                                    |
|assert and log result|{$isSearchSaved}|for assert type|POS|with comment|Assert saved search exists                               |in page|Verify Saved Search|
|If                   |{$isSearchSaved}                                                                                                                       |
|variable             |title           |equals  to                                                                                                            |
|get  text  from      |(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'title')]                                                                    |
|variable             |summary         |equals  to                                                                                                            |
|get  text  from      |(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'summary')]                                                                  |
|assert and log result|COMMENT         |for assert type|NA |with comment|Saved search details: Title: {$title} Summary: {$summary}|in page|Saved Search       |
|End If                                                                                                                                                       |



!|Scenario            |login on save                                                                                                                   |
|wait  for  element   |.//iframe[contains(@id,'popupIframe')]                                                                                          |
|select               |.//iframe[contains(@id,'popupIframe')]|frame                                                                                    |
|wait  for  element   |.//*[@id='tbxPsd']                                                                                                              |
|type                 |test@123                              |into           |.//*[@id='tbxPsd']|text field                                            |
|click                |.//*[@id='LoginForm']/div[2]/div[2]/div[4]/span[text()='Login']                                                                 |
|pause  execution  for|3                                     |seconds                                                                                  |
|select               |Default                               |frame                                                                                    |
|variable             |loginSuccess                          |equals  to                                                                               |
|wait  for  element   |.//div[text()='Manage your search']                                                                                             |
|assert and log result|{$loginSuccess}                       |for assert type|POS               |with comment|Assert login success|in page|Verify Login|


!|Scenario            |close save popup modal             |
|wait  for  element   |.//span[text()='Save this search:']|
|pause  execution  for|4             |seconds             |
|click                |.//span[@title='Close']            |
|pause  execution  for|4             |seconds             |

!|Scenario            |get list source                                                                                                     |
|variable             |isMls                                    |equals  to                                                                |
|verify page contains |.//*[@id='disclaimerContainer']/div/ul/li|element                                                                   |
|If                   |{$isMls}                                                                                                            |
|variable             |source                                   |equals  to                                                                |
|get  text  from      |.//*[@id='disclaimerContainer']/div/ul/li                                                                           |
|scroll to            |.//*[@id='disclaimerContainer']/div/ul/li                                                                           |
|assert and log result|COMMENT                                  |for assert type|NA|with comment|List source: {$source}|in page|List Source|
|End If                                                                                                                                    |

!|Scenario                   |remove saved search                                                                                                                 |
|click                       |(.//div[@class='saved ng-scope'])[1]//i[@class='icon-trash-o']                                                                      |
|pause  execution  for       |2                   |seconds                                                                                                        |
|assert and log result       |COMMENT             |for assert type|POS|with comment|Saved search removed from the list                |in page|Remove Saved Search|
|click                       |.//*[@id='menus']//i[contains(@class,'icon-user')]                                                                                  |
|pause  execution  for       |1                   |seconds                                                                                                        |
|click                       |.//*[@id='menus']//a[text()='Saved searches']                                                                                       |
|set page time out           |1                                                                                                                                   |
|variable                    |savedSearchExists   |equals  to                                                                                                     |
|verify page contains element|(.//div[@class='saved ng-scope'])[1]//div[contains(@class,'title')]                                                                 |
|set page time out           |30                                                                                                                                  |
|assert and log result       |{$savedSearchExists}|for assert type|NEG|with comment|Assert saved item is removed and page is refreshed|in page|Verify Saved Search|



!|Scenario            |add filter conditions                                                                                         |
|set price range                                                                                                                     |
|click                |.//label[@for='bedrooms2']                                                                                    |
|click                |.//*[@id='moreFilterBtn']                                                                                     |
|pause  execution  for|3       |seconds                                                                                              |
|click                |.//*[@id='condos']/../i                                                                                       |
|click                |.//label[@for='bath2']                                                                                        |
|select               |1,000   |from           |.//select[@name='minHouseSize']                                                      |
|select               |2,000   |from           |.//select[@name='maxHouseSize']                                                      |
|select               |1+garage|from           |.//select[@name='garage']                                                            |
|select               |1980    |from           |.//select[@name='minYearBuild']                                                      |
|select               |2010    |from           |.//select[@name='maxYearBuild']                                                      |
|assert and log result|COMMENT |for assert type|NA|with comment|After entering advanced filter conditions|in page|Add advanced filter|
|click                |.//*[@id='applyFilters']                                                                                      |

!|Scenario           |handle modal                      |
|variable            |modalExists            |equals  to|
|verify page contains|.//*[@id='returnToMap']|element   |
|If                  |{$modalExists}                    |
|click               |.//*[@id='returnToMap']           |
|End If                                                 |

!|Scenario            |set price range                                                                                                                          |
|scroll to            |.//*[@id='slider-range']/span[2]                                                                                                         |
|click                |.//*[@id='slider-range']/span[2]                                                                                                         |
|loop                 |5       |times                                                                                                                           |
|send robot           |LEFT    |key                                                                                                                             |
|focus and click      |.//*[@id='slider-range']/span[2]                                                                                                         |
|pause  execution  for|3       |seconds                                                                                                                         |
|end  loop                                                                                                                                                      |
|click                |.//*[@id='slider-range']/span[1]                                                                                                         |
|loop                 |5       |times                                                                                                                           |
|send robot           |RIGHT   |key                                                                                                                             |
|focus and click      |.//*[@id='slider-range']/span[1]                                                                                                         |
|pause  execution  for|3       |seconds                                                                                                                         |
|end  loop                                                                                                                                                      |
|variable             |maxPrice|equals  to                                                                                                                      |
|get  text  from      |.//*[@id='priceSlider']/span[2]                                                                                                          |
|variable             |minPrice|equals  to                                                                                                                      |
|get  text  from      |.//*[@id='priceSlider']/span[1]                                                                                                          |
|assert and log result|COMMENT |for assert type|NA|with comment|After setting price range: Max Price: {$maxPrice} Min Price: {$minPrice}|in page|Set Price Range|
>>>>>>> 6454c2b47d604dc5a0abcfb523ce48bed667bc79
