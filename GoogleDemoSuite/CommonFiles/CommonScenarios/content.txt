'''Scenarios for initializing the reports, open browser, url'''
!|Scenario             |set result name _ at path _|resultname, pagepath                                     |
|test File             |@resultname                |Of Type|XML|At Path|./Custom_Reports|With Style|demostyle|
|variable              |suitePath                  |equals to                                                |
|get Absolute Page Path|@pagepath                                                                            |

!|Scenario  |log result _  with comment _ in page _|result,comment,page|
|test result|ID=@page&&RES=@result&&CMT=@comment                       |

!|Scenario            |start browser and open url _|url                                                                   |
|start browser        |firefox                                                                                            |
|maximize browser                                                                                                         |
|open URL             |@url                                                                                               |
|assert and log result|COMMENT                     |for assert type|NA|with comment|Opened the URL: @url|in page|Home Page|

!|Scenario            |login with username _ and password _|username,password                                                                                         |
|type                 |@username                           |into           |{$LOGIN.usernamefield}|text field                                                         |
|set page time out    |1                                                                                                                                              |
|variable             |passwordExists                      |equals  to                                                                                                |
|verify page contains |{$LOGIN.passwordfield}              |element                                                                                                   |
|set page time out    |30                                                                                                                                             |
|If                   |{$passwordExists}                                                                                                                              |
|type                 |@password                           |into           |{$LOGIN.passwordfield}|text field                                                         |
|assert and log result|COMMENT                             |for assert type|NA                    |with comment|After entering username and password|in page|Home Page|
|click                |{$LOGIN.loginbutton}                                                                                                                           |
|Else                                                                                                                                                                 |
|assert and log result|COMMENT                             |for assert type|NA                    |with comment|After entering password             |in page|Home Page|
|click                |{$LOGIN.nextbutton}                                                                                                                            |
|wait  for  element   |{$LOGIN.passwordfield}                                                                                                                         |
|type                 |@password                           |into           |{$LOGIN.passwordfield}|text field                                                         |
|assert and log result|COMMENT                             |for assert type|NA                    |with comment|After entering password             |in page|Home Page|
|click                |{$LOGIN.loginbutton}                                                                                                                           |
|End If                                                                                                                                                               |

!|Scenario            |verify login action                                                                                         |
|variable             |loginSuccess   |equals to                                                                                   |
|wait for element     |{$INBOX.composebutton}                                                                                      |
|assert and log result|{$loginSuccess}|for assert type|POS|with comment|Assert logged in to Gmail successfully|in page|Verify Login|

'''Scenario for assertion and logging result to custom report'''
!|Scenario      |assert and log result _ for assert type _ with comment _ in page _|result, asserttype, comment, page    |
|variable       |isComment                                                         |equals  to                           |
|verify  whether|comment                                                           |is  present  in   |@result    |string|
|variable       |isPositiveAssert                                                  |equals  to                           |
|verify  whether|POS                                                               |is  present  in   |@asserttype|string|
|variable       |isNegativeAssert                                                  |equals  to                           |
|verify  whether|NEG                                                               |is  present  in   |@asserttype|string|
|If             |{$isComment}                                                                                            |
|test result    |ID=@page&&RES=@result&&CMT=@comment                                                                     |
|Else                                                                                                                    |
|If             |{$isPositiveAssert}                                                                                     |
|variable       |isPass                                                            |equals  to                           |
|assert         |@result                                                           |true with message |@comment          |
|test result    |ID=@page&&RES={$isPass}&&CMT=@comment                                                                   |
|Else If        |{$isNegativeAssert}                                                                                     |
|variable       |isPass                                                            |equals  to                           |
|assert         |@result                                                           |false with message|@comment          |
|test result    |ID=@page&&RES={$isPass}&&CMT=@comment                                                                   |
|End If                                                                                                                  |
|End If                                                                                                                  |
